# Examples
Following page contains description of each Blueprint XML examples.

## 01 Hello World
This example show very simple flow of data using Apache Camel.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.osgi.org/xmlns/blueprint/v1.0.0 https://www.osgi.org/xmlns/blueprint/v1.0.0/blueprint.xsd
      http://camel.apache.org/schema/blueprint
      http://camel.apache.org/schema/blueprint/camel-blueprint.xsd">
    <camelContext id="Hello-World" xmlns="http://camel.apache.org/schema/blueprint">
    <route id="myRoute">
      <from uri="timer://foo?fixedRate=true&amp;period=5000"/>
      <setBody>
        <simple>Hello from blueprint route</simple>
      </setBody>
      <log message="Received payload: ${body}"/>
    </route>
  </camelContext>
</blueprint>
```
### Detials
#### Header
Each Blueprint XML must contain <blueprint> element with proper references to Camel and OSGi schemas
```xml
<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.osgi.org/xmlns/blueprint/v1.0.0 https://www.osgi.org/xmlns/blueprint/v1.0.0/blueprint.xsd
      http://camel.apache.org/schema/blueprint
      http://camel.apache.org/schema/blueprint/camel-blueprint.xsd">
```

#### CamelContext
To define camel route you need to first define Camel context. 
Each Camel context can contain multiply routes. 

```xml
<camelContext id="Hello-World" xmlns="http://camel.apache.org/schema/blueprint">
```

As you can use Blueprint XML as well as Spring XML DSL in definition of routes, in Camel context you need to define with one you will be using by referring to it in xmlns clause

#### Camel route
Each route have to be enclosed in <route></route> tags

##### From
```xml
    <route id="myRoute">
      <from uri="timer://foo?fixedRate=true&amp;period=5000"/>
      <setBody>
        <simple>Hello from blueprint route</simple>
      </setBody>
      <log message="Received payload: ${body}"/>
    </route>
```
Each route have to start with <from uri="..." /> tag. This tag defines what will start the route. 
There is many different options for starting route. See Camel guide for more details. 

In above example route is stated using timer that runs once per 5000ms

##### Set body
<setbody> is one of simplest components. It's main and only task is to set value of message body to defined value.
Ther is many method to do that. <simple> tag indicates that body value will be set to plain text value. In Camel guild you can find other mthods that can be used in <setBody> e.g. scripts etc.

##### Log 
<log> another basic component of Camel is <log> that put defined message in to system logs.
As you can see on example log support variables (${body}). Please see Camel guide for more informations about build in variables and how to delace and use your own variables.

### 02 File transfer using Camel
This example show how to automatically move any files from one folder to second folder using Camel

```xml
<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.osgi.org/xmlns/blueprint/v1.0.0 https://www.osgi.org/xmlns/blueprint/v1.0.0/blueprint.xsd                            http://camel.apache.org/schema/blueprint http://camel.apache.org/schema/blueprint/camel-blueprint.xsd">
    <camelContext id="File-Transfer-Example" xmlns="http://camel.apache.org/schema/blueprint">
        <route id="File-to-File">
            <from id="Inbox" uri="file:data/inbox/FT?noop=true"/>
            <to id="Outbox" uri="file:data/outbox/FT"/>
            <log id="Log" message="File processed"/>
        </route>
    </camelContext>
</blueprint>
```
#### Header & Camel context
As in previous example file starts with prooper header information and declaration of Camel Context.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.osgi.org/xmlns/blueprint/v1.0.0 https://www.osgi.org/xmlns/blueprint/v1.0.0/blueprint.xsd                            http://camel.apache.org/schema/blueprint http://camel.apache.org/schema/blueprint/camel-blueprint.xsd">
    <camelContext id="File-Transfer-Example" xmlns="http://camel.apache.org/schema/blueprint">
```

#### Camel route
In this example we will ou again, known elements of <from>, <to> and <log>.

```xml
        <route id="File-to-File">
            <from id="Inbox" uri="file:data/inbox/FT?noop=true"/>
            <to id="Outbox" uri="file:data/outbox/FT"/>
            <log id="Log" message="File processed"/>
        </route>
```
But because we want to monitor a folder in _uri_ attribute we will put a definition for camel-file component.

```
file:<path>?<option>
```
For more information about camel-file component vist http://camel.apache.org/file2.html

#### Result
After starting this example Camel will wait for any file to be uploaded or created in data/inbox/FT folder.
When the file will be created in that folder it will be automatically copied to data/outbox/FT folder.
During the copying process camel will create special _.lock_ file that will be used to mark file as being under locking and should not be used by any other process.

### 03 File transfer using Camel and AMQ
This example is very similar to previous one. The main difference between them is in:
1. Usage of ActiveMQ Broker as a transfer medium
2. Assumption that files that will be transferred are xml files containing proper elements

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans
  xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:amq="http://activemq.apache.org/schema/core"
  xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
       http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd
       http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd">

  <bean id="activemq" class="org.apache.activemq.camel.component.ActiveMQComponent">
    <property name="brokerURL" value="tcp://localhost:61616"/>
    <property name="userName" value="hxgnminadmin" />
    <property name="password" value="Hex$%$1212@" />
  </bean>

  <camelContext xmlns="http://camel.apache.org/schema/spring">
    <route id="firstRoute">
      <from uri="file:data/inbox/FAT?noop=true"/>
      <to uri="activemq:personnel.records"/>
      <to uri="activemq:browse.me"/>
    </route>
    <route id="secondRoute">
      <from uri="activemq:personnel.records"/>
      <choice>
        <when>
          <xpath>/person/city = 'London'</xpath>
          <to uri="file:data/outbox/FAT/messages/uk"/>
        </when>
        <otherwise>
          <to uri="file:data/outbox/FAT/others"/>
        </otherwise>
      </choice>
    </route>
  </camelContext>
</beans>
```

#### ActiveMQ configuration
Before sending any message over ActiveMQ we need to declare it's location, usere and password that we will be using for connection.
This is done using <bean> tag.

```xml
 <bean id="activemq" class="org.apache.activemq.camel.component.ActiveMQComponent">
    <property name="brokerURL" value="tcp://localhost:61616"/>
    <property name="userName" value="hxgnminadmin" />
    <property name="password" value="Hex$%$1212@" />
  </bean>
```

<bean> tag is a general tag used for many different configurations (not only ActiveMQ).
It's goal is to give information to Camel that _class_ will be used in the routing.

```xml
<bean id="activemq" class="org.apache.activemq.camel.component.ActiveMQComponent">
```

When the <bean> is called by Camel it requires some configuration. In this case:
 * _brokerURL_ - address of ActiveMQ broker
 * _userName_ - user to log in with to ActiveMQ
 * _password_ - password for the user

All of that information are passed to _class_ using <property> tags.

#### Camel routes
As mentioned before Camel context can contain many routes. This can be observe in this example:

##### First route
```xml
    <route id="firstRoute">
      <from uri="file:data/inbox/FAT?noop=true"/>
      <to uri="activemq:personnel.records"/>
      <to uri="activemq:browse.me"/>
    </route>
```
This route checks for files in _data/inbox/FAT_ folder. When new file will be uploaded Camel will send a file in to two ActiveMQ queues:
* personal.records
* browse.me

##### Second route
Second route is waiting for any new messages on _personal.records_ queue (<from> tag)

```xml
    <route id="secondRoute">
      <from uri="activemq:personnel.records"/>
      <choice>
        <when>
          <xpath>/person/city = 'London'</xpath>
          <to uri="file:data/outbox/FAT/messages/uk"/>
        </when>
        <otherwise>
          <to uri="file:data/outbox/FAT/others"/>
        </otherwise>
      </choice>
    </route>
```
The <choice> tag tels Camel to check for condition _<xpath>/person/city = 'London'</xpath>_.
If xml file contains in _/person/city_ word _London_ file will be directed to _data/outbox/FAT/messages/uk_ folder
Otherwise it will be put to _data/outbox/FAT/others_

#### How to create proper file to send
For this example to work you need to create proper xml file. To validate file use _invoce.xsd_ from example folder.

## Deploying examples
To deploy examples on non-production server you need to copy this selected file in to _deploy_ folder of ConnectivityFabric.
Default location of those files is <user_home>/.hawtio/config/Examples/Camel/<example_folder>. Where:
* <user_home> is home catalog of user that is running ConnectivityFabric
* <example_folder> is folder with example file as show on the page.

After copying file check _Logs_ tab where proper information about deployment will be found.
Also _Camel_ tab should be available in WebConsole and proper route should be created.