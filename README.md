# HxGN-MIN-WebConsole Configuration Wiki

Welcome to Hexagon Mining ConnectivityFabric Configuration Wiki.

Click on the **Create** or **Edit** links on the top right of this page to create or edit new content.

## Content
This content is provided by default with installation of Hexagon Mining ConnectivityFabric.
You can remove all all folders except of _dashboard_ folder that contains all your dashboards.

## Documentation
Documentation folder contain ConnectivityFabric documentation in HTML format. The same documentation in PDF and HTML version can be obtain via installation or from Hexagon Maining web page.

## Examples
Examples folder contains fallowing examples:
1. Camel example (see details in Examples/Camel/README)
    1. 01-HelloWorld - simple example showing how to use Camel BlueprintXML
    1. 02-File-Transfer-Camel-Only - this example show how to use Camel to transfer files between two locations.
    1. 03-File-Transfer-ActiveMQ - this example show how to use Camel and ActiveMQ to transfer XML files and based on it's content make different decisions.
2. Wiki Forms - in this folder you can see how HxGN-MIN-WebConsol wiki can be used to interpret different JSON files in to Web forms.